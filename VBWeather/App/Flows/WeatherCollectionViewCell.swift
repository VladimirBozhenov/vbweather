//
//  WeatherCollectionViewCell.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 15/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit
import Kingfisher

class WeatherCollectionViewCell: UICollectionViewCell {
 
    //MARK: IBOutlet
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var forecastCollectionView: UICollectionView!
    
    //MARK: Property
    
    private var dateFormatter: DateFormatter {
        let dt = DateFormatter()
        dt.dateFormat = "HH:mm"
        return dt
    }
    
    private var dateFormatter2: DateFormatter {
        let dt = DateFormatter()
        dt.dateFormat = "EEEE, HH:mm"
        return dt
    }
    
    var forecastWeather = [Weather]()
    
    //MARK: Methods
    
    func setWeatherCellWith(_ cities: [City]) {
        
        guard let forecastWeatherCity = cities.filter({$0.isCurrent == false}).first else { return }
        self.forecastWeather = (forecastWeatherCity.weathers?.allObjects as! [Weather]).sorted{ $0.date < $1.date }
        
        guard let currentWeatherCity = cities.filter({$0.isCurrent == true}).first else { return }
        let currentWeather = currentWeatherCity.weathers?.allObjects.first as! Weather
        
        DispatchQueue.main.async {
            let date = Date(timeIntervalSince1970: currentWeather.date)
            self.dateLabel.text = self.dateFormatter.string(from: date)
            if let flag = currentWeatherCity.country {
                self.flagImage.kf.setImage(with: URL(string: "https://www.countryflags.io/" + flag + "/shiny/64.png"))
            }
            self.cityLabel.text = currentWeatherCity.cityName
            self.temperatureLabel.text = (currentWeather.temperature < 0 ? "" : "+") + String(format: "%.1f℃", currentWeather.temperature)
            self.descriptLabel.text = currentWeather.descript
            self.maxTempLabel.text = (currentWeather.tempMax < 0 ? "" : "+") + String(format: "%.1f℃", currentWeather.tempMax)
            self.minTempLabel.text = (currentWeather.tempMin < 0 ? "" : "+") + String(format: "%.1f℃", currentWeather.tempMin)
            if let icon = currentWeather.weatherIcon {
                self.weatherIcon.kf.setImage(with: URL(string: "https://openweathermap.org/img/wn/" + icon + "@2x.png"))
            }
            let windDeg = WinDeg.getWindString(deg: currentWeather.windDeg)
            self.windLabel.text = windDeg + " " + String(format: "%.1f", currentWeather.windSpeed) + " m/s"
            self.pressureLabel.text = String(format: "%.1f", currentWeather.pressure / 1.333) + " mm Hg St"
            self.humidityLabel.text = "☔︎ " + String(currentWeather.humidity) + " %"
            let sunrise = Date(timeIntervalSince1970: currentWeather.sunrise)
            self.sunriseLabel.text = "☀︎ " + self.dateFormatter.string(from: sunrise)
            let sunset = Date(timeIntervalSince1970: currentWeather.sunset)
            self.sunsetLabel.text = "☽ " + self.dateFormatter.string(from: sunset)
            
            self.forecastCollectionView.reloadData()
        }
    }
}

extension WeatherCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return forecastWeather.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "forecastCell",
                                                      for: indexPath) as! ForecastCollectionViewCell
        cell.setForecastCellWith(weather: forecastWeather[indexPath.row])
        
        return cell
    }
}
