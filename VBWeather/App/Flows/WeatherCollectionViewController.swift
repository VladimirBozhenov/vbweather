//
//  WeatherCollectionViewController.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 14/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

private let reuseIdentifier = "weatherCell"

class WeatherCollectionViewController: UICollectionViewController, CLLocationManagerDelegate {

    //MARK: - Property -
    
    private let apiService = APIService()
    private let dataService = DataService()
    private let alert = Alert()
    private let locationManager = CLLocationManager()
    private var coordinate = CLLocationCoordinate2D()
    
    private lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: City.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id",
                                                         ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: CoreDataStack.sharedInstance.persistentContainer.viewContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    //MARK: - View LifeCycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.requestLocation()
            locationManager.startUpdatingLocation()
        }
        do {
            try self.fetchedhResultController.performFetch()
            print("COUNT FETCHED FIRST: \(String(describing: self.fetchedhResultController.sections?[0].numberOfObjects))")
        } catch let error  {
            print("ERROR: \(error)")
        }
    }
    
    //MARK: - Methods -
    
    func getWeather(period: WeatherPeriod) {
        self.apiService.getWeatherWith(period: period,
                                       lat: self.coordinate.latitude,
                                       lon: self.coordinate.longitude) { (result) in
                                        switch result {
                                        case .Success(let data):
                                            let isCurrent = period == .current ? true : false
                                            self.dataService.clearData(isCurrent: isCurrent)
                                            self.dataService.saveInCoreDataWith(array: data)
                                        case .Error(let message):
                                            DispatchQueue.main.async {
                                                self.alert.showAlertWith(title: "Error",
                                                                         message: message,
                                                                         in: self)
                                            }
                                        }
        }
    }
    
    //MARK:- CLLocationManager Delegates -
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            break
        case .authorizedWhenInUse:
            manager.requestAlwaysAuthorization()
        case .authorizedAlways:
            break
        @unknown default:
            fatalError()
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        
        self.coordinate = locations.first!.coordinate
        
        DispatchQueue.global().async {
            self.getWeather(period: .current)
            self.getWeather(period: .forecast)
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error) {
        guard let locationError = error as? CLError else { return }
        print(locationError.localizedDescription)
    }
    
    // MARK: - UICollectionViewDataSource -
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        if let numberOfFavorites = (fetchedhResultController.fetchedObjects as? [City])?.filter({$0.isFavorite}).count,
        let numberOfCurrent = (fetchedhResultController.fetchedObjects as? [City])?.filter({$0.isCurrent}).count {
            return numberOfFavorites + numberOfCurrent
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! WeatherCollectionViewCell
        
        guard let cities = fetchedhResultController.fetchedObjects as? [City] else { return UICollectionViewCell() }
        cell.setWeatherCellWith(cities)
        return cell
    }
}

//MARK: - NSFetchedResultsControllerDelegate -

extension WeatherCollectionViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.collectionView.reloadData()
    }
}
