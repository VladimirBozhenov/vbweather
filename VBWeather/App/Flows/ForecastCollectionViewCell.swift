//
//  ForecastCollectionViewCell.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 19/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var weatherIconImage: UIImageView! {
        didSet {
            weatherIconImage.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var temperatureLabel: UILabel! {
        didSet {
            temperatureLabel.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.translatesAutoresizingMaskIntoConstraints = false
        }
    }
    
    let insets: CGFloat = 8.0
    
    override func layoutSubviews() {
        temperatureLabelFrame()
        dateLabelFrame()
        weatherIconFrame()
    }
    
    
    func getLabelSize(text: String,
                      font: UIFont) -> CGSize {
        let maxWidth = bounds.width - insets * 2
        let textBlock = CGSize(width: maxWidth,
                               height: CGFloat.greatestFiniteMagnitude)
        let rect = text.boundingRect(with: textBlock,
                                     options: .usesLineFragmentOrigin,
                                     attributes: [NSAttributedString.Key.font: font],
                                     context: nil)
        let width = Double(rect.size.width)
        let height = Double(rect.size.height)
        let size = CGSize(width: ceil(width),
                          height: ceil(height))
        return size
    }
    
    func temperatureLabelFrame() {
        let weaterLabelSize = getLabelSize(text: temperatureLabel.text!,
                                           font: temperatureLabel.font)
        let weaterLabelX = (bounds.width - weaterLabelSize.width) / 2
        let weaterLabelOrigin =  CGPoint(x: weaterLabelX,
                                         y: insets)
        temperatureLabel.frame = CGRect(origin: weaterLabelOrigin,
                                        size: weaterLabelSize)
    }
    
    func dateLabelFrame() {
        let timeLabelSize = getLabelSize(text: dateLabel.text!,
                                         font: dateLabel.font)
        let timeLabelX = (bounds.width - timeLabelSize.width) / 2
        let timeLabelY = bounds.height - timeLabelSize.height - insets
        let timeLabelOrigin =  CGPoint(x: timeLabelX,
                                       y: timeLabelY)
        dateLabel.frame = CGRect(origin: timeLabelOrigin,
                                 size: timeLabelSize)
    }
    
    func weatherIconFrame() {
        let iconSideLinght: CGFloat = 50
        let iconSize = CGSize(width: iconSideLinght,
                              height: iconSideLinght)
        let iconOrigin = CGPoint(x: bounds.midX - iconSideLinght / 2,
                                 y: bounds.midY - iconSideLinght / 2)
        weatherIconImage.frame = CGRect(origin: iconOrigin,
                                        size: iconSize)
    }


    //MARK: Property
    
    private var dateFormatter: DateFormatter {
        let dt = DateFormatter()
        dt.dateFormat = "EEEE, HH:mm"
        return dt
    }
    
    //MARK: Methods
    
    func setForecastCellWith(weather: Weather) {
        
        DispatchQueue.main.async {
            let date = Date(timeIntervalSince1970: weather.date)
            self.dateLabel.text = self.dateFormatter.string(from: date)
            self.temperatureLabel.text = (weather.temperature < 0 ? "" : "+") + String(format: "%.1f℃", weather.temperature)
            if let icon = weather.weatherIcon {
                self.weatherIconImage.kf.setImage(with: URL(string: "https://openweathermap.org/img/wn/" + icon + "@2x.png"))
            }
            self.temperatureLabelFrame()
            self.dateLabelFrame()
            self.weatherIconFrame()
        }
    }
}
