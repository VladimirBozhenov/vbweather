//
//  City+CoreDataProperties.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 16/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//
//

import Foundation
import CoreData


extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var cityName: String?
    @NSManaged public var country: String?
    @NSManaged public var id: Int32
    @NSManaged public var isFavorite: Bool
    @NSManaged public var isCurrent: Bool
    @NSManaged public var weathers: NSSet?

}

// MARK: Generated accessors for weathers
extension City {

    @objc(addWeathersObject:)
    @NSManaged public func addToWeathers(_ value: Weather)

    @objc(removeWeathersObject:)
    @NSManaged public func removeFromWeathers(_ value: Weather)

    @objc(addWeathers:)
    @NSManaged public func addToWeathers(_ values: NSSet)

    @objc(removeWeathers:)
    @NSManaged public func removeFromWeathers(_ values: NSSet)

}
