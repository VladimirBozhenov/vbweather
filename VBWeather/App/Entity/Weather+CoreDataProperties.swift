//
//  Weather+CoreDataProperties.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 19/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//
//

import Foundation
import CoreData


extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather")
    }

    @NSManaged public var date: Double
    @NSManaged public var descript: String?
    @NSManaged public var humidity: Int16
    @NSManaged public var pressure: Double
    @NSManaged public var sunrise: Double
    @NSManaged public var sunset: Double
    @NSManaged public var temperature: Double
    @NSManaged public var tempMax: Double
    @NSManaged public var tempMin: Double
    @NSManaged public var weatherIcon: String?
    @NSManaged public var windDeg: Double
    @NSManaged public var windSpeed: Double
    @NSManaged public var isCurrent: Bool
    @NSManaged public var city: City?

}
