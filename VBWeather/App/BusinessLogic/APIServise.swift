//
//  APIServise.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 13/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

class APIService {
    
    //MARK: - Methods -
    
    func getWeatherWith(period: WeatherPeriod,
                        lat: Double,
                        lon: Double,
                        completion: @escaping (Result<[[String: AnyObject]]>) -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = period.rawValue
        
        urlComponents.queryItems = [
            URLQueryItem(name: "lat", value: String(lat)),
            URLQueryItem(name: "lon", value: String(lon)),
            URLQueryItem(name: "units", value: "metric"),
            URLQueryItem(name: "lang", value: "ru"),
            URLQueryItem(name: "appId", value: "0dbcf1020ccb667bf244aad6e558577f")
        ]
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        guard urlComponents.url != nil else { return completion(.Error("Invalid URL, we can't update you feed")) }
        
        session.dataTask(with: request) { (data, response, error) in
            guard error == nil else { return completion(.Error(error!.localizedDescription)) }
            guard let data = data else { return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))}
            do {
                if let json = try JSONSerialization.jsonObject(with: data,
                                                               options: [.mutableContainers]) as? [String: AnyObject] {
                    let itemsJsonArray = [json]

                    DispatchQueue.main.async {
                        completion(.Success(itemsJsonArray))
                    }
                }
            } catch
                let error {
                    return completion(.Error(error.localizedDescription))
            }
            }.resume()
    }
}
