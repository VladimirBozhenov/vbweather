//
//  DataService.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 14/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import CoreData

class DataService {
    
    private let parseService = ParseService()
    
    func saveInCoreDataWith(array: [[String: AnyObject]]) {
        
        let city = parseService.createCityEntityFrom(dictionary: array[0]) as? City
        var weathers = [Weather]()
        
        if let weatherArray = array.first?["list"] as? [[String: AnyObject]] {
            weathers = weatherArray.map{parseService.createWeatherEntityFrom(dictionary: $0)} as! [Weather]
        } else {
            weathers = array.map{parseService.createWeatherEntityFrom(dictionary: $0)} as! [Weather]
        }
        
        weathers.forEach { city?.addToWeathers($0) }

        do {
            try CoreDataStack.sharedInstance.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    func clearData(isCurrent: Bool) {
        do {
            
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
            fetchRequest.predicate = NSPredicate(format: "SELF.isCurrent == %i", isCurrent)
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                CoreDataStack.sharedInstance.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
}
