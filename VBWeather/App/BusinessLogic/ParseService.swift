//
//  ParseService.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 13/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation
import CoreData

class ParseService {
    
    private let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
    
    func createCityEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
        if let cityEntity = NSEntityDescription.insertNewObject(forEntityName: "City",
                                                                into: context) as? City {
            
            cityEntity.cityName = dictionary["name"] as? String ?? dictionary["city"]?["name"] as? String
            cityEntity.id = dictionary["id"] as? Int32 ?? (dictionary["city"]?["id"] as? Int32)!
            cityEntity.country = dictionary["sys"]?["country"] as? String ?? dictionary["city"]?["country"] as? String
            
            cityEntity.isCurrent = (dictionary["city"] != nil) ? false : true
            cityEntity.isFavorite = false
            
            return cityEntity
        }
        return nil
    }
    
    func createWeatherEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
        
        if let weatherEntity = NSEntityDescription.insertNewObject(forEntityName: "Weather",
                                                                   into: context) as? Weather {
            
            weatherEntity.date = (dictionary["dt"] as? Double)!
            
            let weatherDictionary = dictionary["weather"] as? [[String: AnyObject]]
            weatherEntity.descript = weatherDictionary?[0]["description"] as? String
            weatherEntity.weatherIcon = weatherDictionary?[0]["icon"] as? String
            
            let mainDictionary = dictionary["main"] as? [String: AnyObject]
            weatherEntity.humidity = (mainDictionary?["humidity"] as? Int16)!
            weatherEntity.pressure = (mainDictionary?["pressure"] as? Double)!
            weatherEntity.temperature = (mainDictionary?["temp"] as? Double)!
            weatherEntity.tempMax = (mainDictionary?["temp_max"] as? Double)!
            weatherEntity.tempMin = (mainDictionary?["temp_min"] as? Double)!
            
            let windDictionary = dictionary["wind"] as? [String: AnyObject]
            weatherEntity.windSpeed = (windDictionary?["speed"] as? Double)!
            weatherEntity.windDeg = (windDictionary?["deg"] as? Double) ?? 0.0
            
            let sysDictionary = dictionary["sys"] as? [String: AnyObject]
            weatherEntity.sunrise = sysDictionary?["sunrise"] as? Double ?? 0.0
            weatherEntity.sunset = sysDictionary?["sunset"] as? Double ?? 0.0
            
            weatherEntity.isCurrent = (sysDictionary?["sunrise"] != nil) ? true : false
            
            return weatherEntity
        }
        return nil
    }
}
