//
//  WeatherRequest.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 18/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

enum WeatherPeriod: String {
    case current = "/data/2.5/weather"
    case forecast = "/data/2.5/forecast"
}
