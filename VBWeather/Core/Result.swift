//
//  Result.swift
//  VBWeather
//
//  Created by Vladimir Bozhenov on 13/07/2019.
//  Copyright © 2019 Vladimir Bozhenov. All rights reserved.
//

import Foundation

enum Result <T>{
    case Success(T)
    case Error(String)
}
